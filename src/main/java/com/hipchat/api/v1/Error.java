
package com.hipchat.api.v1;

public interface Error {

    public static final String GENERIC_ERROR_MESSAGE = "An unknown error occurred while processing your request";
    public static final String REQUIRED_PARAMETER_AUTHTOKEN = "An authentication token is required for accessing the HipChat server";
    public static final String REQUIRED_PARAMETER_ID = "Parameter 'id' is required";
    public static final String REQUIRED_PARAMETER_NAME = "Parameter 'name' is required";
    public static final String REQUIRED_PARAMETER_EMAILADDRESS = "Parameter 'emailAddress' is required";
    public static final String REQUIRED_PARAMETER_OWNERID = "Parameter 'ownerId' is required";
    public static final String REQUIRED_PARAMETER_ROOMID = "Parameter 'roomId' is required";

    public static final String HIPCHAT_ERROR_NAME_INVALID = "Invalid name: no_last_name";
    public static final String HIPCHAT_ERROR_EMAIL_EXISTS = "Email already in use";
    public static final String HIPCHAT_ERROR_MENTIONNAME_EXISTS = "mention_name already in use";
    public static final String HIPCHAT_ERROR_ROOM_EXISTS = "Failed to create room: Another room exists with that name.";
    
    // ----------------------------------------------------------------------------------------------- Public methods

    public int getCode();
    public void setCode(int code);
    public String getType();
    public void setType(String type);
    public String getMessage();
    public void setMessage(String message);
    
    // ----------------------------------------------------------------------------------------------- Private methods


    // ----------------------------------------------------------------------------------------------- Private Getters & Setters

    // ----------------------------------------------------------------------------------------------- Getters & Setters


}
