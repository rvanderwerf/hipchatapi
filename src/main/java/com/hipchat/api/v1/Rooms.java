
package com.hipchat.api.v1;

import java.util.List;

public interface Rooms {
    
    public Room get(int id);
    public List<Room> getList();
    
    public boolean create(String name, int ownerId);
    public boolean create(String name, int ownerId, String topic);
    public boolean create(String name, int ownerId, String topic, boolean privateRoom);
    public boolean create(String name, int ownerId, String topic, boolean privateRoom, boolean allowGuests);

    public boolean delete(int id);
    
}
