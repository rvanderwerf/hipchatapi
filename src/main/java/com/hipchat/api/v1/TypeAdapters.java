
package com.hipchat.api.v1;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.TimeZone;

public class TypeAdapters {
    
    public static final TypeAdapter<Boolean> booleanAsIntAdapter = new TypeAdapter<Boolean>() {

        @Override 
        public void write(JsonWriter writer, Boolean t) throws IOException {
            if (t == null) {
                writer.nullValue();
            } else {
                writer.value(t);
            }
        }
      
        @Override 
        public Boolean read(JsonReader reader) throws IOException {
            JsonToken peek = reader.peek();
            switch (peek) {
                case BOOLEAN:
                    return reader.nextBoolean();
                case NULL:
                    reader.nextNull();
                    return null;
                case NUMBER:
                    return reader.nextInt() != 0;
                case STRING:
                    return Boolean.parseBoolean(reader.nextString());
                default:
                    HipChatApiImpl.getInstance().setLastErrorException(new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek));
                    throw (IllegalStateException) HipChatApiImpl.getInstance().getLastErrorException();
            }
        }

    };   
    
    public static final TypeAdapter<TimeZone> timezoneAsStringAdapter = new TypeAdapter<TimeZone>() {

        @Override 
        public void write(JsonWriter writer, TimeZone t) throws IOException {
            if (t == null) {
                writer.nullValue();
            } else {
                writer.value(t.getID());
            }
        }
      
        @Override 
        public TimeZone read(JsonReader reader) throws IOException {
            JsonToken peek = reader.peek();
            switch (peek) {
                case NULL:
                    reader.nextNull();
                    return null;
                case STRING:
                    return TimeZone.getTimeZone(reader.nextString());
                default:
                    HipChatApiImpl.getInstance().setLastErrorException(new IllegalStateException("Expected STRING but was " + peek));
                    throw (IllegalStateException) HipChatApiImpl.getInstance().getLastErrorException();
            }
        }

    };   
    
    
    public static final JsonDeserializer<Date> dateAsUnixTimestamp = new JsonDeserializer<Date>() {

        @Override
        public Date deserialize(JsonElement je, Type type, JsonDeserializationContext jdc) throws JsonParseException {
            // http://stackoverflow.com/questions/3371326/java-date-from-unix-timestamp
            Long timestamp = je.getAsLong();
            if(timestamp > 0) {
                Date result = new Date(timestamp * 1000);
                return result;
            } else {
                return null;
            }
        }
        
    };
    
}
